#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
:mod: module
:author: FIL - Faculté des Sciences et Technologies -  Univ. Lille <http://portail.fil.univ-lille1.fr>_
:date: avril 2019

Diverses fonctions de création de listes d'entiers

"""
import random
from timeit import timeit
import pylab
from tris import *
def cree_liste_croissante(n):
    '''
    :param n: (int) nombre d'entiers souhaités
    :return: (list) liste des entiers de 0 à n - 1
    :CU: aucune (si n <= 0, la liste obtenue est vide)
    :Exemples:

    >>> cree_liste_croissante(6)
    [0, 1, 2, 3, 4, 5]
    >>> cree_liste_croissante(0)
    []
    '''
    return [k for k in range(n)]


def cree_liste_decroissante(n):
    '''
    :param n: (int) nombre d'entiers souhaités
    :return: (list) liste des entiers de n - 1 à 0
    :CU: aucune (si n <= 0, la liste obtenue est vide)
    :Exemples:

    >>> cree_liste_decroissante(6)
    [5, 4, 3, 2, 1, 0]
    >>> cree_liste_decroissante(0)
    []
    '''
    return [(n - 1 - k) for k in range(n)]

def cree_liste_melangee(n):
    '''
    :param n: (int) nombre d'entiers souhaités
    :return: (list) liste des entiers de n - 1 à 0
    :CU: aucune (si n <= 0, la liste obtenue est vide)
    :Exemples:

    >>> n = 6
    >>> l = cree_liste_melangee(n)
    >>> len(l) == n
    True
    >>> all(k in l for k in range(n))
    True
    '''
    l = cree_liste_croissante(n)
    random.shuffle(l)
    return l

def cree_liste_melangee2(n, maxi):
    '''
    :param n: (int) nombre d'entiers souhaités
    :param maxi: (int) valeur maximale des entiers souhaités
    :return: (list) liste de n entiers choisis au hasard entre 0 et maxi (inclus)
    :CU:  maxi >= 0 (si n <= 0, la liste obtenue est vide)
    :Exemples:

    >>> n = 6
    >>> maxi = 10
    >>> l = cree_liste_melangee2(n, maxi)
    >>> len(l) == n
    True
    >>> all(0 <= k <= maxi for k in l)
    True
    '''
    return [random.randrange(maxi + 1) for _ in range(n)]

if __name__ == '__main__':
   import doctest
   doctest.testmod(optionflags=doctest.NORMALIZE_WHITESPACE | doctest.ELLIPSIS, verbose=False)

def temps(taillemax):
    """
    renvoie la mesure de temps pour des listes de toutes les longueurs comprises entre 1 et cette valeur maximale
    :param taillemax:(int) taille maximale des listes à trier
    :return:(list) la liste des temps
    >>> temps(50)
    [9.238100028596818e-07, 9.641799988457934e-07, 2.1148599989828655e-06, 3.034900000784546e-06, 4.145880011492409e-06, 5.37251999048749e-06, 6.624880006711464e-06, 8.678890008013696e-06, 1.0232079985144082e-05, 1.0627659976307769e-05, 1.2373939971439541e-05, 1.4328130018839147e-05, 1.5258339990396053e-05, 1.7347909997624812e-05, 1.8846409984689673e-05, 2.123261998349335e-05, 2.2897489980095998e-05, 2.560886998253409e-05, 2.7948689967161046e-05, 2.988569001900032e-05, 3.276642000855645e-05, 3.498672998830443e-05, 3.802909999649273e-05, 4.072146999533288e-05, 4.3202189990552144e-05, 4.6145320011419246e-05, 4.9135519984702116e-05, 5.227539000770775e-05, 5.542295999475755e-05, 5.867808002221864e-05, 6.20697299746098e-05, 6.541175000165822e-05, 6.905067999468884e-05, 7.269577999977628e-05, 7.656483001483138e-05, 8.04297800277709e-05, 8.43818400244345e-05, 8.845717999065527e-05, 9.296738997363718e-05, 9.678413996880408e-05, 0.0001010509699699469, 0.00010542618998442777, 0.00011002908002410549, 0.00011230148000322515, 0.0001167649999842979, 0.0001227273100084858, 0.0001268684700335143, 0.00013163698000425938, 0.00013613862000056543, 0.00014130221999948843]
    
    """
    l=[]
    for i in range(taillemax):
        l.append(timeit(setup='from tris import tri_select; from listes import cree_liste_melangee',
        stmt=f'tri_select(cree_liste_melangee({i+1}))',
        number=100)/100)
    return l

def temps2(taillemax,nomfonctiondetri):
    """
    renvoie la mesure de temps pour des listes de toutes les longueurs comprises entre 1 et cette valeur maximale pour la fonction de tri
    :param taillemax:(int) taille maximale des listes à trier
    :param fonction de tri:(str) nom de la fonction
    :return:(list) la liste des temps
    >>> temps(50,tri_select)
    [9.238100028596818e-07, 9.641799988457934e-07, 2.1148599989828655e-06, 3.034900000784546e-06, 4.145880011492409e-06, 5.37251999048749e-06, 6.624880006711464e-06, 8.678890008013696e-06, 1.0232079985144082e-05, 1.0627659976307769e-05, 1.2373939971439541e-05, 1.4328130018839147e-05, 1.5258339990396053e-05, 1.7347909997624812e-05, 1.8846409984689673e-05, 2.123261998349335e-05, 2.2897489980095998e-05, 2.560886998253409e-05, 2.7948689967161046e-05, 2.988569001900032e-05, 3.276642000855645e-05, 3.498672998830443e-05, 3.802909999649273e-05, 4.072146999533288e-05, 4.3202189990552144e-05, 4.6145320011419246e-05, 4.9135519984702116e-05, 5.227539000770775e-05, 5.542295999475755e-05, 5.867808002221864e-05, 6.20697299746098e-05, 6.541175000165822e-05, 6.905067999468884e-05, 7.269577999977628e-05, 7.656483001483138e-05, 8.04297800277709e-05, 8.43818400244345e-05, 8.845717999065527e-05, 9.296738997363718e-05, 9.678413996880408e-05, 0.0001010509699699469, 0.00010542618998442777, 0.00011002908002410549, 0.00011230148000322515, 0.0001167649999842979, 0.0001227273100084858, 0.0001268684700335143, 0.00013163698000425938, 0.00013613862000056543, 0.00014130221999948843]
    
    """
    l=[]
    for i in range(taillemax):
        l.append(timeit(setup=f'from tris import {nomfonctiondetri}; from listes import cree_liste_melangee',
        stmt=f'{nomfonctiondetri}(cree_liste_melangee({i+1}))',
        number=100)/100)
    return l

def temps3(taillemax,nomfonctiondetri,sens="croissant"):
    """
    renvoie la mesure de temps pour des listes de toutes les longueurs comprises entre 1 et cette valeur maximale pour la fonction de tri
    :param taillemax:(int) taille maximale des listes à trier
    :param fonction de tri:(str) nom de la fonction
    :return:(list) la liste des temps
    >>> temps3(50,tri_select,compare_decroissant)
    [9.238100028596818e-07, 9.641799988457934e-07, 2.1148599989828655e-06, 3.034900000784546e-06, 4.145880011492409e-06, 5.37251999048749e-06, 6.624880006711464e-06, 8.678890008013696e-06, 1.0232079985144082e-05, 1.0627659976307769e-05, 1.2373939971439541e-05, 1.4328130018839147e-05, 1.5258339990396053e-05, 1.7347909997624812e-05, 1.8846409984689673e-05, 2.123261998349335e-05, 2.2897489980095998e-05, 2.560886998253409e-05, 2.7948689967161046e-05, 2.988569001900032e-05, 3.276642000855645e-05, 3.498672998830443e-05, 3.802909999649273e-05, 4.072146999533288e-05, 4.3202189990552144e-05, 4.6145320011419246e-05, 4.9135519984702116e-05, 5.227539000770775e-05, 5.542295999475755e-05, 5.867808002221864e-05, 6.20697299746098e-05, 6.541175000165822e-05, 6.905067999468884e-05, 7.269577999977628e-05, 7.656483001483138e-05, 8.04297800277709e-05, 8.43818400244345e-05, 8.845717999065527e-05, 9.296738997363718e-05, 9.678413996880408e-05, 0.0001010509699699469, 0.00010542618998442777, 0.00011002908002410549, 0.00011230148000322515, 0.0001167649999842979, 0.0001227273100084858, 0.0001268684700335143, 0.00013163698000425938, 0.00013613862000056543, 0.00014130221999948843]
    
    """
    l=[]
    if sens=="croissant":
        comp="compare"
    else:
        comp="compare_decroissant"
    for i in range(taillemax):
        l.append(timeit(setup=f'from tris import {nomfonctiondetri}; from tris import {comp} ;from listes import cree_liste_melangee',
        stmt=f'{nomfonctiondetri}(cree_liste_melangee({i+1}),comp={comp})',
        number=100)/100)
    return l

def courbe(taillemax):
    """
    produit une courbe du temps d'exécution du tri sélection pour des listes mélangées dont la taille varie de 1 à la valeur du paramètre donné
    :param taillemax:(int)taille maximale des listes à trier
    :return:none
    :effets de bords: lance une fenêtre pylab
    """
    pylab.title('Temps du tri par sélection (pour {:d} essais)'.format(taillemax))
    pylab.xlabel('taille des listes')
    pylab.ylabel('temps en secondes')
    pylab.grid()
    x=[i+1 for i in range(taillemax)]
    y=temps(taillemax)
    pylab.plot(x,y)
    pylab.show()

def courbe2(taillemax,nomfonctiondetri):
    """
    produit une courbe du temps d'exécution du tri choisi pour des listes mélangées dont la taille varie de 1 à la valeur du paramètre donné
    :param taillemax:(int)taille maximale des listes à trier
    :param fonctiondetri
    :return:none
    :effets de bords: lance une fenêtre pylab
    """
    pylab.title(f'Temps du {nomfonctiondetri}(pour'+' {:d} essais)'.format(taillemax))
    pylab.xlabel('taille des listes')
    pylab.ylabel('temps en secondes')
    pylab.grid()
    x=[i+1 for i in range(taillemax)]
    y=temps2(taillemax,nomfonctiondetri)
    pylab.plot(x,y)
    pylab.show()
    
def courbe3(taillemax,nomfonctiondetri,sens="croissant"):
    """
    produit une courbe du temps d'exécution du tri choisi dans le sens choisi
    pour des listes mélangées dont la taille varie de 1 à la valeur du paramètre donné
    :param taillemax:(int)taille maximale des listes à trier
    :param fonctiondetri:(str)
    :param sens:(str)
    :return:none
    :effets de bords: lance une fenêtre pylab
    """
    pylab.title(f'Temps du {nomfonctiondetri}(pour {taillemax:d} essais) dans le sens {sens}')
    pylab.xlabel('taille des listes')
    pylab.ylabel('temps en secondes')
    pylab.grid()
    x=[i+1 for i in range(taillemax)]
    y=temps3(taillemax,nomfonctiondetri,sens)
    pylab.plot(x,y)
    pylab.show()
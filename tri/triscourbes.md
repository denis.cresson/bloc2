## tri select

| croissant                                 | decroissant                                   |
| ----------------------------------------- | --------------------------------------------- |
| ![select_croissant](select_croissant.png) | ![select_decroissant](select_decroissant.png) |

## tri insert

| croissant                                 | decroissant                                   |
| ----------------------------------------- | --------------------------------------------- |
| ![insert_croissant](insert_croissant.png) | ![insert_decroissant](insert_decroissant.png) |

## tri fusion

| croissant                                 | decroissant                                   |
| ----------------------------------------- | --------------------------------------------- |
| ![fusion_croissant](fusion_croissant.png) | ![fusion_decroissant](fusion_decroissant.png) |

## tri sort

| croissant                                 | decroissant                                   |
| ----------------------------------------- | --------------------------------------------- |
| ![sort_croissant](sort_croissant.png) | ![sort_decroissant](sort_decroissant.png) |

## tri rapide

| croissant                                 | decroissant                                   |
| ----------------------------------------- | --------------------------------------------- |
| ![rapide_croissant](rapide_croissant.png) | ![rapide_decroissant](rapide_decroissant.png) |

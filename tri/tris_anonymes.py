#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""

:author: DIU-Bloc2 - Univ. Lille
:date: 2019, mai

Tris de listes

"""

import random
import time

def compare_entier_croissant(a, b):
    """
    :param a: (int) un entier
    :param b: (int) un entier
    :return: (int)  
             * >0  si a est supérieur à b
             * 0 si a est égal à b
             * <0 si a est inférieur à b
    :CU: aucune
    :Exemples:

    >>> compare_entier_croissant(1, 3) < 0
    True
    """
    return a-b

def compare_entier_décroissant(a, b):
    """
    :param a: (int) un entier
    :param b: (int) un entier
    :return: (int) 
             * >0 si a est inférieur à b
             * 0 si a est égal à b
             * <0 si a est supérieur à b
    :CU: aucune
    :Exemples:

    >>> compare_entier_décroissant(1, 3) > 0
    True
    """
    return b-a

def compare_chaine_lexicographique(a, b):
    """
    :param a: (str) un entier
    :param b: (str) un entier
    :return: (int)  
             * >0  si a avant  b
             * 0 si a =  b
             * <0 si a apres b
    :CU: aucune
    :Exemples:

    >>> compare_chaine_lexicographique("a", "b") < 0
    True
    """
    if a<b :
        return -1
    elif a>b:
        return 1
    else:
        return 0
    return 

    
def est_trie(l, comp):
    """
    :param l: (type sequentiel) une séquence 
    :param comp: une fonction de comparaison
    :return: (bool) 
      - True si l est triée
      - False sinon
    :CU: les éléments de l doivent être comparables
    :Exemples:

    >>> est_trie([1, 2, 3, 4], compare_entier_croissant)
    True
    >>> est_trie([1, 2, 4, 3], compare_entier_croissant)
    False
    >>> est_trie([], compare_entier_croissant)
    True
    """
    i = 0
    res = True
    while res and i < len(l) - 1:
        res = comp(l[i], l[i+1]) <= 0
        i += 1
    return res

def cree_liste_melangee(n):
    """
    :param n: (int) Longueur de la liste à créer
    :return: une permutation des n premiers entiers
    :CU: n >= 0
    :Exemples:
        
    >>> l = cree_liste_melangee(5)
    >>> len(l)
    5
    >>> sorted(l) == [0, 1, 2, 3, 4]
    True
    """
    l = list(range(n))
    random.shuffle(l)
    return l


################################################
#                  TRI 1                       #
################################################


def tri_1(l, comp):
    """
    :param l: (list) une liste
    :param comp: une fonction de comparaison
    :CU: Les éléments de l doivent être comparables 
   
    :Effet de bord: modifie la liste l en triant ses éléments selon l'ordre 
        défini par comp, du plus petit au plus grand
    :Exemples:
    
    >>> l = [3, 1, 4, 1, 5, 9, 2]
    >>> tri_1(l, compare_entier_croissant)
    >>> l == [1, 1, 2, 3, 4, 5, 9]
    True
    """
    while not est_trie(l, comp):
        i = random.randint(0,len(l)-2)
        if comp(l[i], l[i+1]) > 0:
            tmp = l[i]
            l[i] = l[i+1]
            l[i+1] = tmp

################################################
#                   TRI 2                      #
################################################

def tri_2(l, comp):
    """
    :param l: (list) une liste
    :param comp: une fonction de comparaison
    :CU: Les éléments de l doivent être comparables 
   
    :Effet de bord: modifie la liste l en triant ses éléments selon l'ordre 
        défini par comp, du plus petit au plus grand
    :Exemples:
    
    >>> l = [3, 1, 4, 1, 5, 9, 2]
    >>> tri_2(l, compare_entier_croissant)
    >>> l == [1, 1, 2, 3, 4, 5, 9]
    True
    """
    n = len(l)
    for i in range(1, n):
        tmp = l[i]
        k = i
        while k >= 1 and comp(tmp, l[k - 1]) < 0 :
            l[k] = l[k - 1]
            k = k - 1
        l[k] = tmp


################################################
#                   TRI 3                      #
################################################

def fais_quelque_chose(l, comp, d, f):
    p = l[d]
    ip = d
    for i in range (d+1, f):
        if comp(p, l[i]) > 0:
            l[ip] = l[i]
            l[i] = l[ip+1]
            ip = ip + 1
    l[ip] = p
    return ip

def tri_3(l, comp, d=0, f=None):
    """
    :param l: (list) une liste
    :param comp: une fonction de comparaison
    :param d: un entier >= 0 et < len(l) (0 par défaut)
    :param f: un entier <= len(l) ou None. None équivaut à len(l)
    :param comp: une fonction de comparaison
    :CU: Les éléments de l doivent être comparables 
   
    :Effet de bord: modifie la liste l en triant ses éléments selon l'ordre 
        défini par comp, du plus petit au plus grand
    :Exemples:
    
    >>> l = [3, 1, 4, 1, 5, 9, 2]
    >>> tri_3(l, compare_entier_croissant)
    >>> l == [1, 1, 2, 3, 4, 5, 9]
    True
    """
    if f is None: f = len(l)
    if f - d > 1:
        ip = fais_quelque_chose(l, comp, d, f)
        tri_3(l, comp, d=d, f=ip)
        tri_3(l, comp, d=ip+1, f=f)

def tri_selection(l,comp):
    """
    implémente un tri par sélection en place
    :param liste:(list)
    :param comp:(function) fonction de comparaison

    :CU: Les éléments de l doivent être comparables 
   
    :Effet de bord: modifie la liste l en triant ses éléments selon l'ordre 
        défini par comp, du plus petit au plus grand
    :Exemples:
    >>> l=cree_liste_melangee(50)
    >>> l
    [5, 40, 8, 24, 42, 44, 33, 10, 34, 25, 16, 35, 45, 32, 39, 3, 11, 43, 9, 31, 38, 15, 1, 27, 2, 4, 26, 14, 28, 36, 7, 20, 12, 6, 29, 21, 49, 37, 18, 22, 41, 19, 48, 13, 46, 30, 47, 23, 17, 0]
    >>> tri_selection(l,compare_entier_croissant)
    >>> l
    [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49]
    """
    for i in range(len(l)):
        val=l[i]
        for j in range(i,len(l)):
            if l[j]<val:
                val,l[j]=l[j],val
        l[i]=val
def fusion(l1,l2,comp):
    """
    fusione deux listes triées
    :param l1:(liste)
    :param l2:(liste)
    :param comp:(function) fonction de comparaison
    :return:(liste) la fusion
    :CU: None
    :exemple:
    >>> fusion([1,3,5,40],[2,4,7,8,9],compare_entier_croissant)
    [1, 2, 3, 4, 5, 7, 8, 9, 40]
    """
    retour=[]
    while len(l1)*len(l2)!=0:
        if comp(l1[0],l2[0])<0:
            retour.append(l1[0])
            l1.pop(0)
        else:
            retour.append(l2[0])
            l2.pop(0)
    if len(l1)==0:
        retour+=l2
    else:
        retour+=l1
    return retour
def tri_fusion(liste,comp):
    """
    retourne la liste triée par un tri fusion
    :param liste:(list)
    :param comp:(function) fonction de comparaison
    :CU: Les éléments de l doivent être comparables 
   
    :Effet de bord: modifie la liste l en triant ses éléments selon l'ordre 
        défini par comp, du plus petit au plus grand
    :Exemples:
    >>> tri_fusion(cree_liste_melangee(100),compare_entier_croissant)
    [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99]
    """
    l=len(liste)
    if l==1:
        return liste
    else:
        return fusion(tri_fusion(liste[0:l//2],comp),tri_fusion(liste[l//2:l],comp),comp)
    
def test_tri(tri,comp,n):
    """
    affiche le temps d'execution du tri choisi sur le nombre de valeurs choisies suivant la comparaison choisie.
    :param tri:(function) fonction de tri
    :param comp:(function) fonction de comparaison
    :param n:(int) taille de la liste aléatoire à trier
    :CU: None
    :exemples:
    >>> test_tri(tri_2,compare_entier_croissant,10000)
    le temps d'execution de ce tri pour 10000 valeurs est de 8.71071720123291 secondes
    >>> test_tri(tri_3,compare_entier_croissant,10000)
    le temps d'execution de ce tri pour 10000 valeurs est de 0.05682086944580078 secondes
    >>> test_tri(tri_selection,compare_entier_croissant,10000)
    le temps d'execution de ce tri pour 10000 valeurs est de 5.366626501083374 secondes
    >>> test_tri(tri_fusion,compare_entier_croissant,10000)
    le temps d'execution de ce tri pour 10000 valeurs est de 0.1047525405883789 secondes
    """
    start_time = time.time()
    tri(cree_liste_melangee(n),comp)
    print(f"le temps d'execution de ce tri pour {n} valeurs est de {time.time()-start_time} secondes")